class ArmorPiece:
    def __init__(self, name, weight, defense):
        self.name = name
        self.weight = weight
        self.defense = defense

helmet_list = [
    ("Academic's Hat", 7, 11),
    ("Bandit's Mask", 3, 6),
    ("Bruiser Helmet", 9, 15),
    ("Cultist Hat", 5, 9),
    ("Crown of the Red Prince", 5, 8),
    ("Dendroid Mask", 3, 7),
    ("Elder Headdress", 6, 6),
    ("Fae Royal Headcover", 8, 15),
    ("Field Medic Hat", 5, 10),
    ("Field Medic Mask", 5, 10),
    ("High Noon Hat", 4, 9),
    ("Knotted Helm", 8, 13),
    ("Labyrinth Headplate", 7, 13),
    ("Leto Mark I Helmet", 15, 29),
    ("Leto Mark II Helmet", 13, 33),
    ("Lodestone Crown", 3, 5),
    ("Navigator's Helm", 8, 12),
    ("Nightstalker Shroud", 6, 11),
    ("Radiant Visage", 8, 14),
    ("Realmwalker Beret", 2, 5),
    ("Red Widow Headdress", 8, 12),
    ("Space Worker Mask", 3, 7),
    ("Technician Helmet", 8, 14),
    ("Trainer Cap", 4, 7),
    ("Void Skull", 8, 15),
    ("No Helmet",0,0)
]

chest_list = [
    ("Academic's Overcoat", 21, 47),
    ("Bandit Jacket", 11, 36),
    ("Bruiser Bodyplate", 29, 57),
    ("Cultist Duster", 17, 46),
    ("Dendroid Chest", 12, 36),
    ("Elder Raiment", 9, 36),
    ("Fae Royal Bodyplate", 27, 57),
    ("Field Medic Overcoat", 16, 45),
    ("High Noon Duds", 19, 48),
    ("Knotted Cage", 28, 53),
    ("Labyrinth Mantle", 20, 45),
    ("Leto Mark I Armor", 45, 101),
    ("Leto Mark II Armor", 44, 107),
    ("Nightstalker Garb", 18, 44),
    ("Radiant Protector", 30, 59),
    ("Realmwalker Tunic", 11, 35),
    ("Red Widow Raiment", 23, 47),
    ("Space Worker Body", 11, 36),
    ("Survivor Overcoat", 19, 46),
    ("Technician Bodyplate", 30, 59),
    ("Trainer Clothes", 18, 46),
    ("Void Carapace", 28, 56),
    ("No Chest",0,0)
]

gloves_list = [
    ("Academic's Gloves", 4, 8),
    ("Bandit Gloves", 2, 6),
    ("Bruiser Gloves", 7, 11),
    ("Cultist Gloves", 6, 5),
    ("Dendroid Grips", 3, 6),
    ("Elder Gloves", 3, 6),
    ("Fae Royal Vambraces", 7, 10),
    ("Field Medic Gloves", 5, 9),
    ("High Noon Armguards", 6, 9),
    ("Knotted Gloves", 6, 11),
    ("Labyrinth Gauntlets", 10, 25),
    ("Leto Mark I Gloves", 11, 20),
    ("Leto Mark II Gloves", 10, 22),
    ("Nightstalker Gloves", 3, 8),
    ("Radiant Bracers", 6, 11),
    ("Realmwalker Gloves", 2, 4),
    ("Red Widow Bracers", 5, 8),
    ("Space Worker Gloves", 2, 6),
    ("Survivor Gloves", 4, 8),
    ("Technician Gloves", 6, 10),
    ("Trainer Bracer", 3, 9),
    ("Void Wraps", 6, 11),
    ("No Gloves",0,0)
]

legs_list = [
    ("Academic's Trousers", 10, 23),
    ("Bandit Trousers", 6, 19),
    ("Bruiser Boots", 15, 27),
    ("Cultist Britches", 10, 23),
    ("Dendroid Leggings", 7, 19),
    ("Elder Leggings", 6, 19),
    ("Fae Royal Greaves", 13, 26),
    ("Field Medic Trousers", 9, 24),
    ("High Noon Soles", 10, 24),
    ("Knotted Greaves", 14, 26),
    ("Labyrinth Treads", 10, 25),
    ("Leto Mark I Leggings", 24, 51),
    ("Leto Mark II Leggings", 23, 55),
    ("Nightstalker Pants", 9, 25),
    ("Radiant Greaves", 17, 29),
    ("Realmwalker Pantaloons", 6, 20),
    ("Red Widow Leggings", 12, 24),
    ("Space Worker Legs", 7, 19),
    ("Survivor Leggings", 10, 24),
    ("Technician Greaves", 14, 29),
    ("Trainer Workboots", 8, 22),
    ("Void Greaves", 15, 28),
    ("No Legs",0,0)
]


armor_lists = [helmet_list, chest_list, legs_list, gloves_list]

def generate_armor_combinations(armor_lists):
    from itertools import product
    combinations = list(product(*armor_lists))
    armor_combinations = []
    
    for combination in combinations:
        armor_pieces = []
        for piece_data in combination:
            name, weight, defense = piece_data
            armor_piece = ArmorPiece(name, weight, defense)
            armor_pieces.append(armor_piece)
        armor_combinations.append(armor_pieces)
    
    return armor_combinations

def calculate_total_weight(armor_combination):
    return sum(piece.weight for piece in armor_combination)

def calculate_total_defense(armor_combination):
    return sum(piece.defense for piece in armor_combination)

def print_armor_combinations_sorted(armor_combinations, min_weight, max_weight):
    sorted_combinations = sorted(armor_combinations, key=lambda combo: (calculate_total_defense(combo), calculate_total_weight(combo)), reverse=True)
    
    for idx, combination in enumerate(sorted_combinations):
        total_weight = calculate_total_weight(combination)
        total_defense = calculate_total_defense(combination)
        
        if min_weight <= total_weight <= max_weight:
            print(f"Combination {idx + 1}: Total Weight = {total_weight}, Total Defense = {total_defense}")
            for piece in combination:
                print(f"- {piece.name} (Weight: {piece.weight}, Defense: {piece.defense})")
            print("\n")

if __name__ == "__main__":
    min_weight = 0  # Replace with your desired minimum weight
    max_weight = 999  # Replace with your desired maximum weight
    
    armor_combinations = generate_armor_combinations(armor_lists)
    print_armor_combinations_sorted(armor_combinations, min_weight, max_weight)
